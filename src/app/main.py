from app.logger import Logger as logging
from fastapi import FastAPI
from app.routers import (
        city,
        picnic,
        user,
        )

app = FastAPI()


app.include_router(city.router)
app.include_router(picnic.router)
app.include_router(user.router)


@app.get('/')
async def root():
    return {'message': 'I am Root'}
