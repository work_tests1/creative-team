import logging
from logging.handlers import TimedRotatingFileHandler as TRFH

LOG_FILE = '/logs/fastapi.log'
Logger = logging.getLogger('fastapi')
Logger.setLevel(logging.INFO)


formatter = logging.Formatter(
        '%(asctime)s : %(filename)s : (%(funcName)s)[LINE:%(lineno)d] : %(levelname)s : %(message)s'
        )

logHandler = TRFH(LOG_FILE,
                  when='D',
                  interval=1,
                  backupCount=0)
logHandler.setLevel(logging.INFO)
logHandler.setFormatter(formatter)


Logger.addHandler(logHandler)
