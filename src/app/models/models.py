from pydantic import BaseModel, validator
from dateutil.parser import parse
from fastapi import Query


__all__ = [
        'RegisterUserRequest',
        'UserModel',
        'PicnicModel',
        'RegToPicnic',
        ]


class RegisterUserRequest(BaseModel):
    name: str
    surname: str
    age: int


class UserModel(BaseModel):
    id: int
    name: str
    surname: str
    age: int

    class Config:
        orm_mode = True


class PicnicModel(BaseModel):
    city_id: int = Query(..., description='ID Города')
    time: str = Query(..., description='Дата и время')

    @validator('time')
    def check_time(cls, date):
        return parse(date)


class RegToPicnic(BaseModel):
    user_id: int = Query(..., description='ID Пользователя')
    picnic_id: int = Query(..., description='ID Пикника')
