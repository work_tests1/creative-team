'''
City Module:
     Основной модуль по управлению всем что касается городов
'''
from app.logger import Logger as logging
from typing import List
from fastapi import HTTPException, Query, APIRouter
from app.database import Session, City
from app.internal import GetWeatherRequest, split_city


router = APIRouter(prefix='/city',
                   tags=['city'])


@router.post('/create',
             summary='Create City',
             description='Создание города по его названию')
async def create_city(city: str = Query(
                description="Название города", default=None)):

    logging.info(f"Rerceived /city/create city - {city}")
    if city is None:
        logging.warning("City parameter is None")
        raise HTTPException(status_code=400,
                            detail='Параметр city должен быть указан')

    check = GetWeatherRequest()

    if not await check.check_existing_city(city):
        logging.error(f"City - {city} Incorrect")
        raise HTTPException(
                status_code=400,
                detail='Параметр city должен быть существующим городом')

    city_object = Session().query(City).filter(
            City.name == city.capitalize()
            ).first()
    if city_object is None:
        city_object = City(name=city.capitalize())
        s = Session()
        s.add(city_object)
        s.commit()

    answer = {
            'id': city_object.id,
            'name': city_object.name,
            'weather': await check.get_weather(city_object.name)
            }
    logging.info(f"Answer - {answer}")
    return answer


@router.get('/get-cities', summary='Get Cities')
async def cities_list(cities: List[str] =
                      Query(None, description='Получение списка городов')):
    """ Получение списка городов
    """
    logging.info(f"Received /city/get-cities parameters - {cities}")
    gw = GetWeatherRequest()

    if not cities:
        cities = Session().query(City).all()
        answer = [{
            'id': city.id,
            'name': city.name,
            'weather': await gw.get_weather(city.name)
            }
                for city in cities]
    else:
        answer = []
        # Если список городов будет передан через запятую в адресной строке
        # /get-cities?cities=msk,spb
        # Возможно это неприемлемый запрос, но в случае если он возникнет,
        # то у меня есть оружие
        cities = split_city(cities)
        for city in cities:
            res = Session().query(City).filter(
                    City.name == city.capitalize()
                    ).first()
            if not res:
                answer.append({'id': 'null',
                               'name': f'{city} not found',
                               'weather': 'null'})
            else:
                answer.append(
                        {
                            'id': res.id,
                            'name': res.name,
                            'weather': await gw.get_weather(res.name)
                        })
    logging.info(f"Answer - {answer}")
    return answer
