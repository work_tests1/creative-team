'''
Picnic module:
    Модуль по управлению пикниками
'''
from app.logger import Logger as logging
import datetime as dt
from fastapi import Query, Depends, APIRouter
from app.database import Session, City, Picnic, PicnicRegistration
from app.models import (
        PicnicModel,
        RegToPicnic,
        )


router = APIRouter(prefix='/picnic',
                   tags=['picnic'])


@router.get('/all', summary='All Picnics')
async def all_picnics(
        datetime: dt.datetime = Query(
                    default=None,
                    description='Время пикника (по умолчанию не задано)'),
        past: bool = Query(
                    default=True,
                    description='Включая уже прошедшие пикники')
                ):
    """ Список всех пикников
    """
    logging.info(f"Received picnic/all date - {datetime} past - {past}")
    # TODO: [ ] Оптимизировать работу с базой данных в запросе `/all-picnics/`
    '''
        Я не свовсем разобрался как составлять сложные запросы при помощи SQLAlchemy
        Я могу сделать запрос при помощи обычного SQL

        SELECT
            p.id, city.name, p.time,
            pr.user_id, pr.name, pr.surname, pr.age
        FROM picnic AS p
        LEFT JOIN city ON
          p.city_id = city.id
        LEFT JOIN (
            SELECT
                pr.picnic_id, pr.user_id,
                u.name, u.surname, u.age
            FROM picnic_registration AS pr
            LEFT JOIN public.user AS u ON
              pr.user_id = u.id
        ) AS pr ON
          p.id = pr.picnic_id
    '''
    picnics = Session().query(Picnic)
    if datetime is not None:
        picnics = picnics.filter(Picnic.time == datetime)
    if not past:
        picnics = picnics.filter(Picnic.time >= dt.datetime.now())

    answer = [{
        'id': pic.id,
        'city': pic.city.name,  # Добавил relationship в Picnic
        'time': pic.time,
        'users': [
            {
                'id': pr.user.id,
                'name': pr.user.name,
                'surname': pr.user.surname,
                'age': pr.user.age,
            }
            for pr in Session().query(PicnicRegistration).filter(
                PicnicRegistration.picnic_id == pic.id)
            ],
    } for pic in picnics]

    logging.info(f"Answer - {answer}")
    return answer


@router.post('/add', summary='Picnic Add')
async def picnic_add(model: PicnicModel = Depends()):
    '''
        Вся валидация проходит в модели PicnicModel
        Она принимает дату как строку и с помощью dateutil.parser.parse()
        Возвращает любую дату в качестве объекта datetime
    '''
    logging.info("Received picnic/add city_id - {}, time - {}"
                 .format(model.city_id, model.time))
    p = Picnic(city_id=model.city_id, time=model.time)
    s = Session()
    s.add(p)
    s.commit()

    answer = {
        'id': p.id,
        'city': Session().query(City).filter(City.id == p.city.id).first().name,
        'time': p.time,
    }
    logging.info(f"Answer - {answer}")
    return answer


@router.post('/register',
             summary='Picnic Registration',)
async def register_to_picnic(model: RegToPicnic = Depends()):
    """ Регистрация пользователя на пикник
    """
    # TODO Зарегать человека на пикник:
    # [v] Получить ID юзера
    # [v] Получить ID пикника
    # [v] Внести в БД запись

    logging.info(
            "Received picnic/regeister UserID - {} PicnicID - {}"
            .format(model.user_id, model.picnic_id))
    pr = PicnicRegistration(
            user_id=model.user_id,
            picnic_id=model.picnic_id,
            )
    s = Session()
    s.add(pr)
    s.commit()

    answer = {
        'user': [pr.user.name, pr.user.surname],
        'picnic_city': pr.picnic.city.name,
        'time': pr.picnic.time,
    }

    logging.info(f"Answer = {answer}")
    return answer
