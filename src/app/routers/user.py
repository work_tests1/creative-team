'''
Module User:
    Основной модуль по управлению функциями для пользователя
'''
from app.logger import Logger as logging
from fastapi import Query, APIRouter
from app.database import Session, User
from app.models import (
        RegisterUserRequest,
        UserModel,
        )


router = APIRouter(prefix='/user',
                   tags=['user'])


@router.get('/users-list/', summary='Get users')
async def users_list(
        min_age: str = Query(description="Минимальный возраст участника",
                             default=None),
        max_age: str = Query(description="Максимальный возраст участника",
                             default=None)):
    """ Список пользователей с возможностью фильтрации
        Фильтрация производится включая указанный возраст
        Допускается фильтрация по обоим полям
    """
    logging.info(
            "Received /user/users-list min_age - {}, max_age - {}"
            .format(min_age, max_age))
    if min_age:
        users = Session().query(User).filter(User.age >= min_age).all()
    if max_age:
        users = Session().query(User).filter(User.age <= max_age).all()
    if not max_age and not min_age:
        users = Session().query(User).all()
    if max_age and min_age:
        users = Session().query(User).filter(
                User.age <= max_age,
                User.age >= min_age
                    ).all()

    answer = [{
        'id': user.id,
        'name': user.name,
        'surname': user.surname,
        'age': user.age,
    } for user in users]

    logging.info(f"Answer - {answer}")
    return answer


@router.post('/register',
             summary='CreateUser',
             response_model=UserModel,)
async def register_user(user: RegisterUserRequest):
    """ Регистрация пользователя
    """
    logging.info(f"Received /user/register User - {user.dict()}")
    user_object = User(**user.dict())
    s = Session()
    s.add(user_object)
    s.commit()
    logging.info(f"User - {user_object} succesfully added")

    return UserModel.from_orm(user_object)
