import asyncio
from collections import namedtuple
from sqlalchemy import (create_engine,
                        Column,
                        Integer,
                        String,
                        ForeignKey,
                        DateTime)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from app.internal import GetWeatherRequest
from app.config import settings as s

# Создание сессии
DATABASE_URI = f'postgresql+psycopg2://{s.db_user}:{s.db_pass}@{s.db_host}:{s.db_port}/{s.database}'
engine = create_engine(DATABASE_URI)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
# Подключение базы (с автоматической генерацией моделей)
Base = declarative_base()


def run_coro(func, *args, **kwargs):
    '''
    Я хотел заставить запускать корутину в синхронном коде,
    но у меня так и не получилось ничего
    В итоге пришлось руками в пакете routers.city вызывать await check.get_weather(city)
    я перепробовал много способов и все свои варианты оставлю закоментированными,
    может кто-то из коллег подскажет
    '''
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:
        loop = None

    if loop and loop.is_running():
        print('Async event loop already running')
        tsk = loop.create_task(func(*args, **kwargs))
        return tsk.result()
    else:
        print('Starting new event loop')
        return asyncio.run(func(*args, **kwargs))


class City(Base):
    """ Город
    """
    __tablename__ = 'city'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True, nullable=False)

    @property
    def weather(self) -> str:
        """ Возвращает текущую погоду в этом городе
        """
        r = GetWeatherRequest()
        # weather = asyncio.create_task(r.get_weather(self.name))
        # asyncio.ensure_future(r.get_weather(self.name))
        # weather = _LOOP.run_until_complete(r.get_weather(self.name))
        weather = run_coro(r.get_weather, self.name)
        # weather = await r.get_weather(self.name)
        # weather = asyncio.run(r.get_weather(self.name))
        return weather

    def __repr__(self):
        return f'<Город "{self.name}">'


class User(Base):
    """ Пользователь
    """
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    surname = Column(String, nullable=False)
    age = Column(Integer, nullable=True)

    def __repr__(self):
        return f'<Пользователь {self.surname} {self.name}>'


class Picnic(Base):
    """ Пикник
    """
    __tablename__ = 'picnic'

    id = Column(Integer, primary_key=True, autoincrement=True)
    city_id = Column(Integer, ForeignKey('city.id'), nullable=False)
    time = Column(DateTime, nullable=False)

    city = relationship('City', backref='cities')

    def __repr__(self):
        return f'<Пикник ID-{self.id} city-{self.city.name} time-{self.time}>'


class PicnicRegistration(Base):
    """ Регистрация пользователя на пикник
    """
    __tablename__ = 'picnic_registration'

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    picnic_id = Column(Integer, ForeignKey('picnic.id'), nullable=False)

    user = relationship('User', backref='picnics')
    picnic = relationship('Picnic', backref='users')

    def __repr__(self):
        return f'<Регистрация {self.id}>'


Base.metadata.create_all(bind=engine)
