from app.logger import Logger as logging
from app.config import settings as s
import aiohttp


WEATHER_API_KEY = s.weather_api


class GetWeatherRequest():
    """
    Выполняет запрос на получение текущей погоды для города
    """

    def __init__(self):
        """ Инициализирует класс
        """
        # self.session = requests.Session()
        # self.session = aiohttp.ClientSession()
        pass

    async def get_weather_url(self, city):
        """ Генерирует url включая в него необходимые параметры
            Args:
                city: Город
            Returns:
        """
        url = 'https://api.openweathermap.org/data/2.5/weather'
        url += '?units=metric'
        url += '&q=' + city
        url += '&appid=' + WEATHER_API_KEY
        logging.debug(f"URL for weather - {url}")
        return url

    async def send_request(self, url, raise_err=False):
        """ Отправляет запрос на сервер
            Args:
                url: Адрес запроса
            Returns:
                r.status: status code - int
                r.json: json data or None
        """
        async with aiohttp.ClientSession(raise_for_status=raise_err) as session:
            async with session.get(url) as r:
                if r.status != 200:
                    logging.warning(f"Weather status - {r.status}")
                    return r.status, None
                return r.status, await r.json()

    async def get_weather_from_response(self, data):
        """ Достает погоду из ответа
            Args:
                response: Ответ, пришедший с сервера
            Returns:
        """
        logging.debug(f"Full data - {data}")
        return data['main']['temp']

    async def get_weather(self, city):
        """ Делает запрос на получение погоды
            Args:
                city: Город
            Returns:

        """
        url = await self.get_weather_url(city)
        status, data = await self.send_request(url)
        if status == 404:
            logging.error(f"WeatherRequest for {city} incorrect")
            return None
        else:
            weather = await self.get_weather_from_response(data)
            logging.info(f"In {city} Temp is {weather}")
            return weather

    async def check_existing_city(self, city):
        """ Проверяет наличие города
            Args:
                city: Название города
            Returns:
        """
        url = await self.get_weather_url(city)
        status, _ = await self.send_request(url)

        if status == 404:
            logging.warning(f"{city} does not exist")
            return False
        if status == 200:
            logging.info(f"{city} exist")
            return True
