from app.logger import Logger as logging


def split_city(cities: list):
    for city in cities:
        if ',' in city:
            logging.info(f"Received Incorrect city - {city}")
            splitted = city.split(',')
            for s in splitted:
                cities.append(s)
            del cities[cities.index(city)]
    return cities
