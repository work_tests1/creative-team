from pydantic import BaseSettings


class _Settings(BaseSettings):
    db_user = 'postgres'
    db_pass = 'password'
    db_host = 'postgres'
    db_port = '5432'
    database = 'testcrt'
    weather_api = '99ba78ee79a2a24bc507362c5288a81b'


settings = _Settings()
