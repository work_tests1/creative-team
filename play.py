import aiohttp
import asyncio

city = 'tiraspol'
WEATHER_API_KEY = '99ba78ee79a2a24bc507362c5288a81b'

url = 'https://api.openweathermap.org/data/2.5/weather'
url += '?units=metric'
url += '&q=' + city
url += '&appid=' + WEATHER_API_KEY


async def runreq():
    async with aiohttp.ClientSession() as s:
        async with s.get(url) as r:
            print(r.status)
            # print(dir(r))
            # data = await r.json()
            # print(data['main']['temp'])
            return await r.json()

async def main():
    res = await runreq()
    print(res)


asyncio.run(main())
